= Index Page

:imagesdir: ../images
:toc:

Documentation is like gardening, you constantly need to nourish it or weed takes over.

image::gardening.png[Gardening, 350]

== First level heading
This is a paragraph

=== Local Links

* link:asciidoc-practices.adoc[AsciiDoc Recommended Practices^]
* link:asciidoc-reference.adoc[AsciiDoc Syntax Reference^]


'''

image::asciidoctor.png[Asciidoc, 30]
Powered By Asciidoctor

