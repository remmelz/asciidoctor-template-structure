
Browser = C:\Program Files\Mozilla Firefox\firefox.exe
Docdir = C:\Users\keesr\Projects

SetKeyDelay, 50

FileSelectFolder, RootFolder, %Docdir% , , Select AsciiDoc root folder
Run %Browser% file:///%RootFolder%/pages/README.adoc

LinkInsert() {

    global RootFolder

    If WinExist("ahk_class Notepad") {

        FileSelectFile, DocFile, 1, ,Select Document, Documents (*.adoc)
        RemovePath = %RootFolder%\pages\
        DocFile := StrReplace(DocFile, RemovePath)

        SetKeyDelay, 10
        Send link:%DocFile%[Link title]
        SetKeyDelay, 50
    }
}

ImageInsert() {

    global RootFolder
    
    If WinExist("ahk_class Notepad") {

        FileSelectFile, ImageFile, 1, ,Select Image, Images (*.png; *.jpg, *.gif)
        RemovePath = %RootFolder%\images\
        ImageFile := StrReplace(ImageFile, RemovePath)

        WinActivate
        SetKeyDelay, 10
        Send image::%ImageFile%[Image title, 200]
        SetKeyDelay, 50
    }
}

InsertTemplate() {

    If WinExist("ahk_class Notepad") {

        WinActivate
        SetKeyDelay, 10

        Send {enter}
        Send :imagesdir: ../images{enter}
        Send :toc:{enter}{enter}
        Send {=} Main Title{enter}{enter}		
        Send A short description about this document{enter}{enter}
        Send {=}{=} First level heading{enter}
        Send This is a paragraph{enter}

        SetKeyDelay, 50
    }

}

NotepadEdit() {

    Send ^c
    ClipFind = %clipboard%
    Sleep 100

    Send ^l
    Send ^c
    ClipFile = %clipboard%
    ClipFile := StrSplit(ClipFile, "#")
    ClipFile := ClipFile[1]
    ClipFile := StrSplit(ClipFile, "///")
    ClipFile := ClipFile[2]

    DocName := StrSplit(ClipFile, "/")
    DocName := DocName[DocName.MaxIndex()]

    WinTitle = %DocName% - Notepad

    If WinExist("ahk_class Notepad") {
        WinActivate
    } else {
        Run, Notepad.exe %ClipFile%
        WinWait %DocName% - Notepad
        WinActivate %DocName% - Notepad
    }

    clipboard := ClipFind
    Sleep 100
    Send ^{home}
    Send ^f
    Send ^v
    Send {enter}
}

RCtrl & g::
FileSelectFolder, RootFolder, %Docdir% , , Select AsciiDoc root folder
Run %Browser% file:///%RootFolder%/pages/README.adoc
return

RCtrl & s::
Send #+s
return

RCtrl & h::
Run https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/
return

RCtrl & o::
FileSelectFile, AsciiDoc, 1, ,Select Page, AsciiDoc (*.adoc)
Run %Browser% %AsciiDoc%
return

RCtrl & e::
NotepadEdit()
return

RCtrl & l::
LinkInsert()
return

RCtrl & i::
ImageInsert()
return

RCtrl & t::
InsertTemplate()
return

RCtrl & b::
Run %RootFolder%
return

