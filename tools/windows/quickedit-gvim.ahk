
Browser = C:\Program Files\Mozilla Firefox\firefox.exe
Editor = C:\PortableApps\gVimPortable\gVimPortable.exe
Docdir = C:\Users\keesr\Projects

SetKeyDelay, 50

FileSelectFolder, RootFolder, %Docdir% , , Select AsciiDoc root folder
Run %Browser% file:///%RootFolder%/pages/README.adoc


AsciidocTable =
(
[header`%,cols=2*] 
|===
|Name of Column 1
|Name of Column 2

|Cell in column 1, row 1
|Cell in column 2, row 1

|Cell in column 1, row 2
|Cell in column 2, row 2
|===
)


LinkInsert() {

    global RootFolder

    If WinExist("ahk_class Vim") {

        FileSelectFile, DocFile, 1, ,Select Document or Attachment,
        
        IfInString, DocFile, \pages\
        {
            RemovePath = %RootFolder%\pages\
            DocFile := StrReplace(DocFile, RemovePath)
            
        } else {
        
            RemovePath = %RootFolder%\
            DocFile := StrReplace(DocFile, RemovePath)
            DocFile = ..\%DocFile%
        }
        
        clipboard = link:%DocFile%[Link title]
        
        WinActivate
        SetKeyDelay, 10
        Send {escape}{escape}i
        Send +{insert}
        
        SetKeyDelay, 50
    }
}

TableInsert() {

    global AsciidocTable

    If WinExist("ahk_class Vim") {

        WinActivate
        SetKeyDelay, 10

        clipboard = %AsciidocTable%

        Send {escape}{escape}i
        Send +{insert}
        
        SetKeyDelay, 50
    }
}

ImageInsert() {

    global RootFolder
    
    If WinExist("ahk_class Vim") {

        FileSelectFile, ImageFile, 1, ,Select Image, Images (*.png; *.jpg, *.gif)
        RemovePath = %RootFolder%\images\
        ImageFile := StrReplace(ImageFile, RemovePath)

        clipboard = image::%ImageFile%[Image title, 200]  // float="right"]
        
        WinActivate
        SetKeyDelay, 10
        Send {escape}{escape}i
        Send +{insert}
        SetKeyDelay, 50
    }
}

EditDocument() {

    Global Editor

    Send ^c
    ClipFind = %clipboard%
    Sleep 100

    Send ^l
    Send ^c
    ClipFile = %clipboard%
    ClipFile := StrSplit(ClipFile, "#")
    ClipFile := ClipFile[1]
    ClipFile := StrSplit(ClipFile, "///")
    ClipFile := ClipFile[2]

    DocName := StrSplit(ClipFile, "/")
    DocName := DocName[DocName.MaxIndex()]

    WinTitle = %DocName% - Notepad

    If WinExist("ahk_class Vim") {
        WinActivate
    } else {
        Run, %Editor% %ClipFile%
        WinWait, ahk_class Vim
        WinActivate, ahk_class Vim
        WinMove, ahk_class Vim, , 1000, 250
    }

    clipboard := ClipFind
    Sleep 100
    Send {escape}/
    Send +{insert}
    Send {enter}
    Send zz
}

RCtrl & g::
FileSelectFolder, RootFolder, %Docdir% , , Select AsciiDoc root folder
Run %Browser% file:///%RootFolder%/pages/README.adoc
return

RCtrl & s::
Send #+s
return

RCtrl & h::
Run https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/
return

RCtrl & o::
FileSelectFile, AsciiDoc, 1, ,Select Page, AsciiDoc (*.adoc)
Run %Browser% %AsciiDoc%
return

RCtrl & e::
EditDocument()
return

RCtrl & t::
TableInsert()
return

RCtrl & l::
LinkInsert()
return

RCtrl & i::
ImageInsert()
return

RCtrl & b::
Run %RootFolder%
return

